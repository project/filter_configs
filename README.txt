Filter Configurations
---------------------
This module adds a search box to the "admin/config" page to provide filter
capability.

Module maintainers
------------------
Malay Nayak 'malaynayak' https://www.drupal.org/u/malaynayak
